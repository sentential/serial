// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.

package serial

import (
	"math"
	"os"
)

// SerialiseFloat32 writes a float32 to the provided file, at the current position,
// returning true on success.
func SerialiseFloat32(file *os.File, u float32) bool {
	b := make([]byte, 1)
	b[0] = byte(SerialisedFloat32)
	count, err := file.Write(b)
	if count != 1 || err != nil {
		return false
	}
	if WriteFloat32(file, u) != nil {
		return false
	}
	return true
}

// WriteFloat32 writes one float32 to the file using WriteUint32.
// It uses math.Float32bits to convert the float to uint32 and thus
// may not be portable (depending entirely on your platforms).
func WriteFloat32(file *os.File, f float32) error {
	u := math.Float32bits(f)
	return WriteUint32(file, u)
}

// ReadFloat32 reads one float32 from the file using ReadUint32.
// It uses math.Float32frombits to convert the uint32 to a float32
// and thus may not be entirely portable.
func ReadFloat32(file *os.File) (f float32, err error) {
	var u uint32
	u, err = ReadUint32(file)
	f = math.Float32frombits(u)
	return f, err
}
