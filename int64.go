// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.

package serial

import (
	"os"
)

// SerialiseInt64 writes a int64 to the provided file, at the current position,
// returning true on success.  A single byte is written prior to the int64 so that
// the deserialiser knows the type.
//
// There is no DeserialiseInt64 function as it is intended to be used with the
// generic Deserialise function.
func SerialiseInt64(file *os.File, u int64) bool {
	b := make([]byte, 1)
	b[0] = byte(SerialisedInt64)
	count, err := file.Write(b)
	if count != 1 || err != nil {
		return false
	}
	if WriteInt64(file, u) != nil {
		return false
	}
	return true
}

// WriteInt64 writes one int64 to a file in an endian-independent
// manner.  It does not write the 'type' byte.
func WriteInt64(file *os.File, u int64) error {
	return WriteUint64(file, uint64(u))
}

// ReadInt64 reads one int64 in an endian-independent manner.
// It does not read the 'type' byte.
func ReadInt64(file *os.File) (i int64, err error) {
	var u uint64
	u, err = ReadUint64(file)
	i = int64(u)
	return
}
