# Serial (Go Serialisation)

This is a trivial library used in the de/serialisation of built-in types.  At present it represents an incomplete library in the phases of development; it's made public for the purposes of testing and general use.

As with other public code, expect updates as they become needed or requested.