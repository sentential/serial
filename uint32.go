// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.

package serial

import (
	"fmt"
	"os"
)

// SerialiseUint32 writes a uint32 to the provided file, at the current position,
// returning true on success.  A single byte is written prior to the uint32 so that
// the deserialiser knows the type.
//
// There is no DeserialiseUint32 function as it is intended to be used with the
// generic Deserialise function.
func SerialiseUint32(file *os.File, u uint32) bool {
	b := make([]byte, 1)
	b[0] = byte(SerialisedUint32)
	count, err := file.Write(b)
	if count != 1 || err != nil {
		return false
	}
	if WriteUint32(file, u) != nil {
		return false
	}
	return true
}

// WriteUint32 writes one uint32 to a file in an endian-independent
// manner.  It does not write the 'type' byte.
func WriteUint32(file *os.File, u uint32) error {
	b := make([]byte, 4)
	b[0] = byte(u>>24) & 0xFF
	b[1] = byte(u>>16) & 0xFF
	b[2] = byte(u>>8) & 0xFF
	b[3] = byte(u) & 0xFF
	count, err := file.Write(b)
	if count != 4 {
		err = fmt.Errorf("count stored is incorrect (expected 4)")
	}
	return err
}

// ReadUint32 reads one uint32 in an endian-independent manner.
// It does not read the 'type' byte.
func ReadUint32(file *os.File) (u uint32, err error) {
	b := make([]byte, 4)
	count, err := file.Read(b)
	if count == 4 && err != nil {
		u = (uint32(b[0]) << 24) + (uint32(b[1]) << 16) + (uint32(b[2]) << 8) + uint32(b[3])
	}
	return
}
