// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.

package serial

import (
	"os"
)

// SerialiseInt32 writes a int32 to the provided file, at the current position,
// returning true on success.  A single byte is written prior to the int32 so that
// the deserialiser knows the type.
//
// There is no DeserialiseInt32 function as it is intended to be used with the
// generic Deserialise function.
func SerialiseInt32(file *os.File, u int32) bool {
	b := make([]byte, 1)
	b[0] = byte(SerialisedInt32)
	count, err := file.Write(b)
	if count != 1 || err != nil {
		return false
	}
	if WriteInt32(file, u) != nil {
		return false
	}
	return true
}

// WriteInt32 writes one int32 to a file in an endian-independent
// manner.  It does not write the 'type' byte.
func WriteInt32(file *os.File, u int32) error {
	return WriteUint32(file, uint32(u))
}

// ReadInt32 reads one int32 in an endian-independent manner.
// It does not read the 'type' byte.
func ReadInt32(file *os.File) (i int32, err error) {
	var u uint32
	u, err = ReadUint32(file)
	i = int32(u)
	return
}
