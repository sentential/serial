// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.
//
// Use of this package is governed by the LICENCE file
// found in the root of the repository:
// - http://bitbucket.org/sentential/serial

package serial

import (
	"os"
)

// A Serialisable object is an object (type, struct, whatever) which contains the ability
// to save and loads its internal types without the aid of the reflect package.
type Serialisable interface {
	// Serialise should serialise the contents of the struct to a given file.
	// It should only return true if the struct's serialisation is considered a success.
	//
	// Serialisation may rely on ordered storage (skipping 'SerialiseX' functions and instead
	// making a preference to use 'WriteX' functions).
	Serialise(file *os.File) bool

	// Deserialise should deserialise the contents of the struct from a given file.
	// It should only return true if the struct's deserialisation is considered a success.
	//
	// Deserialisation order should be maintained if it is expected by serialise; it may
	// skip the use of 'Serialise' types or interpret the trailing byte of 'SerialisedStruct'
	// in unique ways distinct from the rest of the application or library.
	Deserialise(file *os.File) bool
}
