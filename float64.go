// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.

package serial

import (
	"math"
	"os"
)

// SerialiseFloat64 writes a float64 to the provided file, at the current position,
// returning true on success.
func SerialiseFloat64(file *os.File, u float64) bool {
	b := make([]byte, 1)
	b[0] = byte(SerialisedFloat64)
	count, err := file.Write(b)
	if count != 1 || err != nil {
		return false
	}
	if WriteFloat64(file, u) != nil {
		return false
	}
	return true
}

// WriteFloat64 writes one float64 to the file using WriteUint64.
// It uses math.Float64bits to convert the float to uint64 and thus
// may not be portable (depending entirely on your platforms).
func WriteFloat64(file *os.File, f float64) error {
	u := math.Float64bits(f)
	return WriteUint64(file, u)
}

// ReadFloat64 reads one float64 from the file using ReadUint64.
// It uses math.Float64frombits to convert the uint64 to a float64
// and thus may not be entirely portable.
func ReadFloat64(file *os.File) (f float64, err error) {
	var u uint64
	u, err = ReadUint64(file)
	f = math.Float64frombits(u)
	return f, err
}
