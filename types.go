// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.

package serial

// SerialisedType is a single byte which is used for unknown struct values;
// it is designed to be used for handling of unknown structures which will
// be unpacked into an array of interfaces.
type SerialisedType uint8

// Serialised type definitions are provided for the known/used types.
const (
	SerialisedStruct  SerialisedType = iota // Known struct (of some kind)
	SerialisedInt8                          // int8
	SerialisedUint8                         // uint8
	SerialisedInt16                         // int16
	SerialisedUint16                        // uint16
	SerialisedInt32                         // int32
	SerialisedUint32                        // uint32
	SerialisedInt64                         // int64
	SerialisedUint64                        // uint64
	SerialisedFloat32                       // float32
	SerialisedFloat64                       // float64
	SerialisedString                        // string
	SerialisedByte                          // []byte
)
