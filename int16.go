// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.

package serial

import (
	"os"
)

// SerialiseInt16 writes a int16 to the provided file, at the current position,
// returning true on success.  A single byte is written prior to the int16 so that
// the deserialiser knows the type.
//
// There is no DeserialiseInt16 function as it is intended to be used with the
// generic Deserialise function.
func SerialiseInt16(file *os.File, u int16) bool {
	b := make([]byte, 1)
	b[0] = byte(SerialisedInt16)
	count, err := file.Write(b)
	if count != 1 || err != nil {
		return false
	}
	if WriteInt16(file, u) != nil {
		return false
	}
	return true
}

// WriteInt16 writes one int16 to a file in an endian-independent
// manner.  It does not write the 'type' byte.
func WriteInt16(file *os.File, u int16) error {
	return WriteUint16(file, uint16(u))
}

// ReadInt16 reads one int16 in an endian-independent manner.
// It does not read the 'type' byte.
func ReadInt16(file *os.File) (i int16, err error) {
	var u uint16
	u, err = ReadUint16(file)
	i = int16(u)
	return
}
