// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.

package serial

import (
	"fmt"
	"os"
)

// SerialiseUint16 writes a uint16 to the provided file, at the current position,
// returning true on success.  A single byte is written prior to the uint16 so that
// the deserialiser knows the type.
//
// There is no DeserialiseUint16 function as it is intended to be used with the
// generic Deserialise function.
func SerialiseUint16(file *os.File, u uint16) bool {
	b := make([]byte, 1)
	b[0] = byte(SerialisedUint16)
	count, err := file.Write(b)
	if count != 1 || err != nil {
		return false
	}
	if WriteUint16(file, u) != nil {
		return false
	}
	return true
}

// WriteUint16 writes one uint16 to a file in an endian-independent
// manner.  It does not write the 'type' byte.
func WriteUint16(file *os.File, u uint16) error {
	b := make([]byte, 2)
	b[0] = byte(u>>8) & 0xFF
	b[1] = byte(u) & 0xFF
	count, err := file.Write(b)
	if count != 2 {
		err = fmt.Errorf("count stored is incorrect (expected 2)")
	}
	return err
}

// ReadUint16 reads one uint16 in an endian-independent manner.
// It does not read the 'type' byte.
func ReadUint16(file *os.File) (u uint16, err error) {
	b := make([]byte, 2)
	count, err := file.Read(b)
	if count == 2 && err != nil {
		u = (uint16(b[0]) << 8) + uint16(b[1])
	}
	return
}
