// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.

package serial

import (
	"fmt"
	"os"
)

// SerialiseString writes a string to the given file.
func SerialiseString(file *os.File, s string) bool {
	b := make([]byte, 1)
	b[0] = byte(SerialisedString)
	count, err := file.Write(b)
	if count != 1 || err != nil {
		return false
	}
	if WriteString(file, s) != nil {
		return false
	}
	return true
}

// WriteString writes one int32 to a file in an endian-independent
// manner.  It does not write the 'type' byte.
func WriteString(file *os.File, s string) (err error) {
	err = WriteUint32(file, uint32(len(s)))
	if err != nil {
		return nil
	}

	b := []byte(s)
	_, err = file.Write(b)
	return nil
}

// ReadString reads one int32 in an endian-independent manner.
// It does not read the 'type' byte.
func ReadString(file *os.File) (s string, err error) {
	var u uint32
	var count int
	u, err = ReadUint32(file)
	b := make([]byte, u)
	count, err = file.Read(b)
	if uint32(count) != u {
		err = fmt.Errorf("unable to read string")
	}
	return
}
