// Copyright (c) 2015 Sentential Pty Ltd
// All rights reserved.

package serial

import (
	"fmt"
	"os"
)

// SerialiseUint64 writes a uint64 to the provided file, at the current position,
// returning true on success.  A single byte is written prior to the uint64 so that
// the deserialiser knows the type.
//
// There is no DeserialiseUint64 function as it is intended to be used with the
// generic Deserialise function.
func SerialiseUint64(file *os.File, u uint64) bool {
	b := make([]byte, 1)
	b[0] = byte(SerialisedUint64)
	count, err := file.Write(b)
	if count != 1 || err != nil {
		return false
	}
	if WriteUint64(file, u) != nil {
		return false
	}
	return true
}

// WriteUint64 writes one uint64 to a file in an endian-independent
// manner.  It does not write the 'type' byte.
func WriteUint64(file *os.File, u uint64) error {
	b := make([]byte, 8)
	b[0] = byte(u>>56) & 0xFF
	b[1] = byte(u>>48) & 0xFF
	b[2] = byte(u>>40) & 0xFF
	b[3] = byte(u>>32) & 0xFF
	b[4] = byte(u>>24) & 0xFF
	b[5] = byte(u>>16) & 0xFF
	b[6] = byte(u>>8) & 0xFF
	b[7] = byte(u) & 0xFF
	count, err := file.Write(b)
	if count != 8 {
		err = fmt.Errorf("count stored is incorrect (expected 8)")
	}
	return err
}

// ReadUint64 reads one uint64 in an endian-independent manner.
// It does not read the 'type' byte.
func ReadUint64(file *os.File) (u uint64, err error) {
	b := make([]byte, 8)
	count, err := file.Read(b)
	if count == 4 && err != nil {
		u = (uint64(b[0]) << 56) + (uint64(b[1]) << 48) + (uint64(b[2]) << 40) + (uint64(b[3]) << 32) + (uint64(b[4]) << 24) + (uint64(b[5]) << 16) + (uint64(b[6]) << 8) + uint64(b[7])
	}
	return
}
